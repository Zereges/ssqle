﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace SSQLE
{
    /// <summary>
    /// Main application class.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// Custom <c>ApplicationContext</c>, that handles opening and closing of new forms.
        /// </summary>
        internal static AppContext ApplicationContext;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(ApplicationContext = new AppContext(new ConnectionManager()));
        }

        /// <summary>
        /// Loads the settings from given stream.
        /// </summary>
        /// <param name="stream">Stream containing XML settings to be loaded into the applicaiton.</param>
        /// <seealso cref="Settings"/>.
        public static void LoadSettings(Stream stream)
        {
            Settings = Settings.LoadFromXml(stream);
        }

        /// <summary>
        /// Language of the application.
        /// </summary>
        public static Lang<EnglishLang> Language { get; private set; } = Lang<EnglishLang>.Instance;

        /// <summary>
        /// Settings of the application.
        /// </summary>
        public static Settings Settings { get; private set; }

        /// <summary>
        /// Crypto of the application. Uses <see cref="TrivialCrypto"/>.
        /// </summary>
        public static ICrypto Crypto { get; private set; } = new TrivialCrypto();
    }


    /// <summary>
    /// Custom <c>ApplicationContext</c>, that handles showing new forms and closing of old ones.
    /// It keeps track of forms present in the application, and once all forms are closed, it
    /// peacefully closes the application.
    /// </summary>
    internal class AppContext : ApplicationContext
    {
        /// <summary>
        /// Set of forms present in the application.
        /// </summary>
        private readonly ISet<Form> forms;

        /// <summary>
        /// Creates new instance of <see cref="AppContext"/> with initial form being shown.
        /// </summary>
        /// <param name="form">First form to be shown in the application.</param>
        public AppContext(Form form)
        {
            form.FormClosing += OnFormClosed;
            form.Show();
            MainForm = form;
            forms = new HashSet<Form> { form };
        }

        /// <summary>
        /// Custom handler that is called whenever <c>Form</c>, that this <see cref="AppContext"/> brought
        /// to existence using <see cref="ShowForm"/>, closes itself.
        /// </summary>
        /// <param name="sender">Form, that triggered the closed event.</param>
        /// <param name="e">Event arguments.</param>
        private void OnFormClosed(object sender, EventArgs e)
        {
            forms.Remove((Form) sender);
            if (forms.Count == 0)
                ExitThread();
        }

        /// <summary>
        /// Shows form by calling <see cref="Form.Show()"/>, and registers <see cref="OnFormClosed"/> handler
        /// to that form.
        /// </summary>
        /// <param name="form"><c>Form</c> to be shown.</param>
        /// <param name="setAsMain">Flag to distinguish between main form and supporting form.</param>
        public void ShowForm(Form form, bool setAsMain = false)
        {
            forms.Add(form);
            form.FormClosed += OnFormClosed;
            form.Show();
            if (setAsMain)
                MainForm = form;
        }
    }
}
