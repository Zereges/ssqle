﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace SSQLE
{
    using OracleAccess = Oracle.ManagedDataAccess.Client;
    /// <summary>
    /// Class, used to represent results from executed query by the database.
    /// </summary>
    public class QueryResult
    {
        /// <summary>
        /// Number of records affected by the query.
        /// </summary>
        private readonly int recordsAffected;

        /// <summary>
        /// Time, that took the query to execute.
        /// </summary>
        private readonly TimeSpan elapsed;
        
        /// <summary>
        /// Column names from the SELECT statement.
        /// </summary>
        private readonly IList<string> columns;

        /// <summary>
        /// Maximum column widths.
        /// </summary>
        private readonly int[] columnSizes;

        /// <summary>
        /// <c>Type</c>s of the columns from the SELECT statement.
        /// </summary>
        private readonly IList<Type> columnTypes;

        /// <summary>
        /// Table representing data of the SELECT statement.
        /// </summary>
        private readonly IList<IList<object>> data;


        /// <summary>
        /// Constructs new instance of <see cref="QueryResult"/> with given arguments.
        /// </summary>
        /// <param name="elapsed">Time, that took the query to execute.</param>
        /// <param name="recordsAffected">Records affected by the query.</param>
        /// <param name="columns">Column names from the SELECT statement.</param>
        /// <param name="columnSizes">Maximum column widths.</param>
        /// <param name="columnTypes"><c>Type</c>s of the columns from the SELECT statement.</param>
        /// <param name="data">Table representing data of the SELECT statement.</param>
        private QueryResult(TimeSpan elapsed, int recordsAffected, IList<string> columns, int[] columnSizes, IList<Type> columnTypes, IList<IList<object>> data)
        {
            this.columns = columns;
            this.columnSizes = columnSizes;
            this.columnTypes = columnTypes;
            this.data = data;
            this.elapsed = elapsed;
            this.recordsAffected = recordsAffected;
        }

        /// <summary>
        /// Time, that took the query to execute, in seconds. 
        /// </summary>
        public string ElapsedTime => elapsed.TotalSeconds.ToString(CultureInfo.CurrentUICulture);

        /// <summary>
        /// Method used to update maximum of <see cref="number"/> passed by reference.
        /// </summary>
        /// <param name="number">Number to potentionaly update.</param>
        /// <param name="newValue">Value to update <paramref name="number"/> with.</param>
        private static void UpdateMaximum(ref int number, int newValue)
        {
            number = Math.Max(number, newValue);
        }

        /// <summary>
        /// Factory method used to create instance of <see cref="QueryResult"/> from underlaying ODP.NET
        /// <c>OracleDataReader</c>.
        /// </summary>
        /// <param name="reader">Reader, from which result shall be taken.</param>
        /// <param name="elapsed">Time, that took the query to execute.</param>
        /// <returns>Newly constructed <see cref="QueryResult"/> with coresponding data.</returns>
        public static QueryResult FromOracleDataReader(OracleAccess.OracleDataReader reader, TimeSpan elapsed)
        {
            if (reader.RecordsAffected == -1) // select stmt
            {
                IList<string> columns = new List<string>();
                IList<Type> columnTypes = new List<Type>();
                int[] columnSizes = new int[reader.FieldCount];
                for (int i = 0; i < reader.FieldCount; ++i)
                {
                    string columnName = reader.GetName(i);
                    columns.Add(columnName);
                    columnTypes.Add(reader.GetFieldType(i));
                    UpdateMaximum(ref columnSizes[i], columnName.Length);
                }

                IList<IList<object>> data = new List<IList<object>>();
                while (reader.Read())
                {
                    IList<object> row = new List<object>();
                    for (int i = 0; i < reader.FieldCount; ++i)
                    {
                        object value = reader.GetOracleValue(i);
                        row.Add(value);
                        UpdateMaximum(ref columnSizes[i], value.ToString().Length);
                    }
                    data.Add(row);
                }
                return new QueryResult(elapsed, reader.RecordsAffected, columns, columnSizes, columnTypes, data);
            }
            return new QueryResult(elapsed, reader.RecordsAffected, null, null, null, null);
        }

        /// <summary>
        /// Item delimiter in CSV format. (<c>,</c>)
        /// </summary>
        private const string CsvDelimiter = ",";
        
        /// <summary>
        /// Item delimiter in SQL expressions. (<c>, </c>)
        /// </summary>
        private const string SqlExpressionDelimiter = ", ";

        /// <summary>
        /// Quotation mark used in SQL statements to ensure string literal. (<c>'</c>)
        /// </summary>
        private const string QuotationMark = "'";

        /// <summary>
        /// Saves the result of the query to CSV format
        /// </summary>
        /// <param name="writer"><c>TextWriter</c> to write CSV to.</param>
        public void SaveToCsv(TextWriter writer)
        {
            // ToDo: either escaping of comma, or quoting (and additional escaping)
            writer.WriteLine(string.Join(CsvDelimiter, columns));
            foreach (var row in data)
                writer.WriteLine(string.Join(CsvDelimiter, row));
        }

        /// <summary>
        /// Saves the result of the query to SQL INSERT statement format.
        /// </summary>
        /// <param name="writer"><c>TextWriter</c> to write to.</param>
        public void SaveToInsertStatement(TextWriter writer)
        {
            writer.WriteLine($"INSERT INTO tablename ({string.Join(SqlExpressionDelimiter, columns)}) VALUES");
            for (int i = 0; i < data.Count; i++)
            {
                writer.Write('(');
                for (int j = 0; j < data[i].Count; ++j)
                {
                    // more complex check?
                    if (columnTypes[j] == typeof(string))
                        writer.Write(QuotationMark);
                    writer.Write(data[i][j]);
                    if (columnTypes[j] == typeof(string))
                        writer.Write(QuotationMark);

                    if (j != data[i].Count - 1)
                        writer.Write(SqlExpressionDelimiter);
                }
                writer.Write(')');
                writer.WriteLine(i == data.Count - 1 ? ";" : ",");
            }
        }

        /// <summary>
        /// Class used to visualise results of the query.
        /// </summary>
        private class QueryResultBuilder
        {
            /// <summary>
            /// <c>StringBuilder</c> to build the string.
            /// </summary>
            private readonly StringBuilder builder = new StringBuilder();
            
            /// <summary>
            /// Reference to width of single columns.
            /// </summary>
            private readonly int[] columnSizes;

            /// <summary>
            /// Constructs new <see cref="QueryResultBuilder"/> with given column widths.
            /// </summary>
            /// <param name="columnSizes">Widths of single columns.</param>
            public QueryResultBuilder(int[] columnSizes)
            {
                this.columnSizes = columnSizes;
            }

            /// <summary>
            /// Space character.
            /// </summary>
            private const char Space = ' ';

            /// <summary>
            /// Separator of columns. Maybe shall be changed to TAB of visible width 1 point.
            /// </summary>
            private const char ColSeparator = ' ';

            /// <summary>
            /// Separator of header (names of columns) and real data.
            /// </summary>
            private const char RuleCharacter = '-';

            /// <summary>
            /// Appends column names from the argument to the results view.
            /// </summary>
            /// <param name="columns">Columns of the query.</param>
            public void AppendColumns(IList<string> columns)
            {
                // column names
                for (int i = 0; i < columnSizes.Length; ++i)
                {
                    string str = columns[i];
                    builder.Append(new string(Space, columnSizes[i] - str.Length));
                    builder.Append(str);
                    if (i != columnSizes.Length - 1)
                        builder.Append(ColSeparator);
                }
                builder.Append(Environment.NewLine);

                foreach (int size in columnSizes)
                    builder.Append(new string(RuleCharacter, size));
                builder.Append(Environment.NewLine);
            }

            /// <summary>
            /// Appends single row to the results view.
            /// </summary>
            /// <param name="values"></param>
            public void AppendRow(IList<object> values)
            {
                for (int i = 0; i < columnSizes.Length; ++i)
                {
                    string str = values[i].ToString();
                    builder.Append(new string(Space, columnSizes[i] - str.Length));
                    builder.Append(str);
                    if (i != columnSizes.Length - 1)
                        builder.Append(ColSeparator);
                }
                builder.Append(Environment.NewLine);
            }

            /// <summary>
            /// Returns textual representation of query result.
            /// </summary>
            /// <returns>Textual representation of query result.</returns>
            public override string ToString()
            {
                return builder.ToString();
            }
        }

        /// <summary>
        /// Returns textual representation of query result.
        /// </summary>
        /// <returns>Textual representation of query result.</returns>
        /// <remarks>
        /// Shows either columns with selected values if used to visualise SELECT statement
        /// or number of affected records by the statement in other cases.
        /// </remarks>
        public override string ToString()
        {
            if (recordsAffected != -1) // not select stmt
                return Program.Language[LangStrings.RecordsAffected, recordsAffected];
            QueryResultBuilder builder = new QueryResultBuilder(columnSizes);
            builder.AppendColumns(columns);
            foreach (IList<object> row in data)
            {
                builder.AppendRow(row);
            }
            return builder.ToString();
        }
    }
}
