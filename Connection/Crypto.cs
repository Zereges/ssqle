﻿namespace SSQLE
{
    /// <summary>
    /// Interface providing mechanism for textual encryption and decryption.
    /// </summary>
    internal interface ICrypto
    {
        string Encrypt(string str);
        string Decrypt(string str);
    }

    /// <summary>
    /// Class used for trivial enryption and decryption of passwords, that are stored in memory and/or in settings file.
    /// </summary>
    /// This implementation uses trivial encryption using identity function. That is encypt(string) == string.
    /// 
    /// One can improve the implementation by using some third party library like OpenSSL.
    internal class TrivialCrypto : ICrypto
    {
        /// <summary>
        /// Decrypts the <see cref="str"/>.
        /// </summary>
        /// <param name="str">String to decrypt.</param>
        /// <returns>Decrypted string.</returns>
        public string Decrypt(string str)
        {
            return str;
        }

        /// <summary>
        /// Encrypts the <see cref="str"/>.
        /// </summary>
        /// <param name="str">String to encrypt.</param>
        /// <returns>Encrypted string.</returns>
        public string Encrypt(string str)
        {
            return str;
        }
    }
}
