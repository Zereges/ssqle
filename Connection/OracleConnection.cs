using System;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

namespace SSQLE
{
    using OracleAccess = Oracle.ManagedDataAccess.Client;

    /// <summary>
    /// Interface providing facilities to connect to a relational database.
    /// </summary>
    public interface IConnection : IDisposable
    {
        /// <summary>
        /// Opens the connection to the databse.
        /// </summary>
        void Open();

        /// <summary>
        /// Closes previously opened connection to the databse.
        /// </summary>
        void Close();

        /// <summary>
        /// Lets underlaying database execute the <paramref name="query"/> and returns its results.
        /// </summary>
        /// <param name="query">Query to be passed to the databse for execution.</param>
        /// <returns><see cref="QueryResult"/> representing retrieved values from the underlaying databse.</returns>
        QueryResult ExecuteQuery(string query);
    }

    /// <summary>
    /// Implementation of connection to Oracle database.
    /// </summary>
    /// This implementation uses native ODP.NET (Oracle Data Provider for .NET) from NuGet packages.
    public class OracleConnection : IConnection
    {
        /// <summary>
        /// Returns connection string to be used as parameter for connecting to underlaying Oracle database.
        /// </summary>
        /// <param name="host">Host to connect to. Can be hostname, IPv4 or IPv6 address or machine name.</param>
        /// <param name="port">Port to connect to.</param>
        /// <param name="sid">SID to connect to.</param>
        /// <param name="user">User to be authenticated as.</param>
        /// <param name="pass">Encrypted password to use for authentication. See <see cref="ICrypto"/>.</param>
        /// <returns>Connection string for ODP.NET implementation.</returns>
        private static string GetConnectionString(string host, string port, string sid, string user, string pass) =>
            $"Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST={host})(PORT={port})))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={sid})));User Id={user};Password={pass};";

        /// <summary>
        /// Underlaying connection.
        /// </summary>
        private OracleAccess.OracleConnection connection;

        private readonly string host;
        private readonly string port;
        private readonly string sid;
        private readonly string user;
        private readonly string pass;

        /// <summary>
        /// Constructs new connection with given arguments.
        /// Does not open the connection yet. Subsequent call to <see cref="Open"/> is required, before executing queries.
        /// </summary>
        /// <param name="host">Host.</param>
        /// <param name="port">Port.</param>
        /// <param name="sid">SID.</param>
        /// <param name="user">User.</param>
        /// <param name="pass">Encrypted password. See <see cref="ICrypto"/>.</param>
        /// 
        /// <seealso cref="GetConnectionString"/>.
        private OracleConnection(string host, string port, string sid, string user, string pass)
        {
            this.host = host;
            this.port = port;
            this.sid = sid;
            this.user = user;
            this.pass = pass;
        }

        /// <summary>
        /// Factory method for creating new instance of <see cref="OracleConnection"/>.
        /// Does not open the connection yet. Subsequent call to <see cref="Open"/> is required, before executing queries.
        /// </summary>
        /// <param name="host">Host.</param>
        /// <param name="port">Port.</param>
        /// <param name="sid">SID.</param>
        /// <param name="user">User.</param>
        /// <param name="pass">Encrypted password. See <see cref="ICrypto"/>.</param>
        /// 
        /// <returns>New instance of <see cref="OracleConnection"/>, which is not opened yet.</returns>
        public static OracleConnection NewConnection(string host, string port, string sid, string user, string pass)
            => new OracleConnection(host, port, sid, user, pass);

        /// <summary>
        /// Opens the connection to the underlaying database.
        /// </summary>
        public void Open()
        {
            connection = new OracleAccess.OracleConnection(GetConnectionString(host, port, sid, user, Program.Crypto.Decrypt(pass)));
            connection.Open();
        }

        /// <summary>
        /// Closes the connection to the underlaying database.
        /// </summary>
        public void Close()
        {
            Dispose();
        }

        /// <summary>
        /// Executes the query into underlaying database and retrieves the results.
        /// </summary>
        /// <param name="query">Single query to be executed by the underlaying databse.</param>
        /// <returns>Result of the query into the database.</returns>
        /// <remarks>
        /// ODP.NET provider allows execution of only one query at any given time. That means, that <c>;</c> character
        /// is invalid character in the query <c>SELECT * FROM dual;</c>.
        /// 
        /// It however allows forcing database to execute PL/SQL code by wrapping the code in <c>BEGIN END</c> block, which
        /// requires semicolon at the end. Thus if user wants to pass such code block to the underlaying driver, it has to
        /// end with double semicolon.
        /// 
        /// One could improve the implementation by parsing the query and looking for single command to be passed to the database by skipping comments,
        /// string literals and looking for semi colon.
        /// </remarks>
        public QueryResult ExecuteQuery(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return null;

            Stopwatch sw = Stopwatch.StartNew();

            OracleAccess.OracleCommand command = connection.CreateCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = query;
            OracleAccess.OracleDataReader reader;
            try
            {
                reader = command.ExecuteReader();
            }
            catch (Exception ex)
            {
                sw.Stop();

                Program.ApplicationContext.MainForm.BeginInvoke(new Action(() =>
                {
                    MessageBox.Show(Program.Language[LangStrings.QueryNotExecuted, ex.Message], Program.Language[LangStrings.Error]);
                }));
                return null;
            }
            sw.Stop();
            QueryResult result = QueryResult.FromOracleDataReader(reader, sw.Elapsed);
            reader.Close();
            return result;
        }

        /// <summary>
        /// Disposes the resources not freed by GC by calling Close to underlaying database.
        /// </summary>
        public void Dispose()
        {
            connection.Close();
        }
    }
}
