﻿using System.Collections;
using System.Collections.Generic;

namespace SSQLE
{
    /// <summary>
    /// Class representing a history of commands executed during the runtime.
    /// </summary>
    /// It implements <c>IList&lt;string&gt;</c> and is implemented internally using <c>List&lt;string&gt;</c>.
    /// That means, <see cref="RemoveFirst"/> method has linear complexity (instead of assumed O(1), but since
    /// <see cref="maxItems"/> is likely to be in size of hundreds, it should not be an issue in real use case.
    /// 
    /// Implementation could be improved either by using Deque container, or using <c>LinkedList</c> and
    /// maintaining <c>LinkedListNode</c> instead of <see cref="Editor.historyIndex"/>.
    public class CommandsHistory : IList<string>
    {
        /// <summary>
        /// List of lastly used commands.
        /// </summary>
        private readonly IList<string> commands = new List<string>();
        private readonly int maxItems;

        /// <param name="maxItems">Maximal number of commands this instance can store.</param>
        public CommandsHistory(int maxItems)
        {
            this.maxItems = maxItems;
        }

        /// <summary>
        /// Adds new item at the end of the history. If there is not space for that item
        /// (i.e. <c>Count >= maxItems</c>, then the oldest item is removed.
        /// </summary>
        /// <param name="item">Command to be stored.</param>
        /// <seealso cref="RemoveFirst"/>
        public void Add(string item)
        {
            if (Count == maxItems)
                RemoveFirst();
            commands.Add(item);
        }

        /// <summary>
        /// Removes the oldest command from the history.
        /// Has O(maxItems) complexity. See <see cref="CommandsHistory"/> for more information.
        /// </summary>
        private void RemoveFirst() => commands.RemoveAt(0);

        /// <summary>
        /// Removes item from the list.
        /// </summary>
        /// <param name="item">Item to be removed.</param>
        /// <returns>True if item were present in the list, false otherwise.</returns>
        public bool Remove(string item) => commands.Remove(item);

        /// <summary>
        /// Clears the list.
        /// </summary>
        public void Clear() => commands.Clear();
        
        /// <summary>
        /// Checks whether this container contains specified item.
        /// </summary>
        /// <param name="item">Item to chekc for.</param>
        /// <returns>True if contains, false otherwise.</returns>
        public bool Contains(string item) => commands.Contains(item);

        /// <summary>
        /// Copies elements of this container to array pointed by <paramref name="array"/>,
        /// starting at index <paramref name="arrayIndex"/>.
        /// </summary>
        /// <param name="array">Array to copy to.</param>
        /// <param name="arrayIndex">Index in <paramref name="array"/> from which to copy.</param>
        public void CopyTo(string[] array, int arrayIndex) => commands.CopyTo(array, arrayIndex);

        /// <summary>
        /// Count of elements in the container.
        /// </summary>
        public int Count => commands.Count;

        /// <summary>
        /// Checks, whether this is read only container. Always <c>false</c>.
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// Gets the enumerator for the list
        /// </summary>
        /// <returns>Enumerator of the list.</returns>
        public IEnumerator<string> GetEnumerator() => commands.GetEnumerator();

        /// <summary>
        /// Gets the enumerator for the list
        /// </summary>
        /// <returns>Enumerator of the list.</returns>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        
        /// <summary>
        /// Index of specified item.
        /// </summary>
        /// <param name="item">Item, which index is desired.</param>
        /// <returns>Index of such item, or <c>-1</c> if item is not present in the list.</returns>
        public int IndexOf(string item) => commands.IndexOf(item);

        /// <summary>
        /// Inserts an item to the list.
        /// </summary>
        /// <param name="index">Index where to insert.</param>
        /// <param name="item">Item to be inserted.</param>
        public void Insert(int index, string item) => commands.Insert(index, item);

        /// <summary>
        /// Removes item at <paramref name="index"/>.
        /// </summary>
        /// <param name="index">Index of item to be removed.</param>
        public void RemoveAt(int index) => commands.RemoveAt(index);

        /// <summary>
        /// Indexer for the list.
        /// </summary>
        /// <param name="index">Index in the list.</param>
        /// <returns>Command at <paramref name="index"/>.</returns>
        public string this[int index]
        {
            get { return commands[index]; }
            set { commands[index] = value; }
        }
    }
}
