﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Drawing;
using System.Windows.Forms;

namespace SSQLE
{
    /// <summary>
    /// Exception, that is thrown when <see cref="Settings"/> can's be saved.
    /// </summary>
    public class SettingsSaveException : Exception
    {
        /// <summary>
        /// Constructs new instance of <see cref="SettingsSaveException"/>.
        /// </summary>
        /// <param name="message">Base message.</param>
        /// <param name="inner">Inner exception.</param>
        public SettingsSaveException(string message, Exception inner) : base(message, inner)
        {
        }
    }

    /// <summary>
    /// Exception, that is thrown when <see cref="Settings"/> can's be validated. See documentation of settings.xsd on main page.
    /// </summary>
    internal class SettingsLoadException : Exception
    {
        /// <summary>
        /// Constructs new instance of <see cref="SettingsLoadException"/>.
        /// </summary>
        /// <param name="message">Base message.</param>
        /// <param name="inner">Inner exception.</param>
        public SettingsLoadException(string message, Exception inner) : base(message, inner)
        {
        }
    }

    /// <summary>
    /// Represents settings of single connection profile.
    /// </summary>
    internal class Settings
    {
        /// <summary>
        /// Private constructor. Is to be created using <see cref="LoadFromXml"/>.
        /// </summary>
        private Settings()
        {
        }

        /// <summary>
        /// Loads <see cref="Settings"/> from <see cref="stream"/> containing XML document with settings.
        /// </summary>
        /// <param name="stream">Stream containing the XML with settings or <c>null</c>.</param>
        /// <returns>Settings loaded from <see cref="stream"/> or default settings, if <see cref="stream"/> is <c>null</c>.</returns>
        /// <exception cref="SettingsLoadException">
        /// When XML is not valid according to <c>settings.xsd</c>. See documentatin of <c>settings.xsd</c> on main page.
        /// </exception>
        public static Settings LoadFromXml(Stream stream)
        {
            Settings settings = new Settings();
            if (stream == null)
                return settings;

            XmlDocument document = new XmlDocument();

            if (schema == null)
            {
                using (FileStream fileStream = new FileStream(XsdSchema, FileMode.Open))
                    schema = XmlSchema.Read(fileStream, null);
            }
            document.Schemas.Add(schema);

            try
            {
                document.Load(stream);
                document.Validate(null);
            }
            catch (XmlException exception)
            {
                throw new SettingsLoadException(Program.Language[LangStrings.XmlLoadExceptionWellFormed], exception);
            }
            catch (XmlSchemaValidationException exception)
            {
                throw new SettingsLoadException(Program.Language[LangStrings.XmlLoadExceptionValid], exception);
            }

            settings.Name = GetSettingsFromXPath(document, "/settings/name");
            settings.Host = GetSettingsFromXPath(document, "/settings/host");
            settings.User = GetSettingsFromXPath(document, "/settings/user");
            settings.Pass = GetSettingsFromXPath(document, "/settings/pass");
            settings.Sid = GetSettingsFromXPath(document, "/settings/sid");
            settings.Port = GetSettingsFromXPath(document, "/settings/port");

            settings.KeyWord = TextSettings.GetTextSettingsFromXPath(document, "/settings/highlighter/keyword_color");
            settings.Function = TextSettings.GetTextSettingsFromXPath(document, "/settings/highlighter/function_color");
            settings.Comment = TextSettings.GetTextSettingsFromXPath(document, "/settings/highlighter/comment_color");
            settings.StringLiteral = TextSettings.GetTextSettingsFromXPath(document, "/settings/highlighter/stringliteral_color");
            settings.NumberLiteral = TextSettings.GetTextSettingsFromXPath(document, "/settings/highlighter/numberliteral_color");

            settings.CommandsHistory = GetCommandsHistoryFromXPath(document, "/settings/history/command");
            settings.Snippets = GetSnippetsFromXPath(document, "/settings/snippets/query");

            return settings;
        }

        /// <summary>
        /// Returns mapping of saved queries from <see cref="document"/> located using XPath expression.
        /// </summary>
        /// <param name="document"><c>XmlDocument</c> containing settings.</param>
        /// <param name="xpath">XPath expression.</param>
        /// <returns><c>Dictionary&lt;string, string&gt;</c> of saved queries in the profile. Key is name, Value is query.</returns>
        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        private static IDictionary<string, string> GetSnippetsFromXPath(XmlDocument document, string xpath)
        {
            IDictionary<string, string> snippets = new Dictionary<string, string>();
            foreach (XmlNode queryNode in document.SelectNodes(xpath))
            {
                snippets.Add(queryNode.Attributes["name"].InnerText, queryNode.InnerText);
            }
            return snippets;
        }

        /// <summary>
        /// Maximum command to be stored at once in history.
        /// </summary>
        private const int MaxCommandHistory = 100;

        /// <summary>
        /// Returns <see cref="CommandsHistory"/> from <see cref="document"/> located using XPath expression.
        /// </summary>
        /// <param name="document"><c>XmlDocument</c> containing settings.</param>
        /// <param name="xpath">XPath expression.</param>
        /// <returns><see cref="CommandsHistory"/> of the profile.</returns>
        private static CommandsHistory GetCommandsHistoryFromXPath(XmlDocument document, string xpath)
        {
            CommandsHistory history = new CommandsHistory(MaxCommandHistory);
            // ReSharper disable once PossibleNullReferenceException
            foreach (XmlNode commandNode in document.SelectNodes(xpath))
            {
                history.Add(commandNode.InnerText);
            }
            return history;
        }

        #region XmlSupport
        /// <summary>
        /// Wrapper class around <c>XmlDocument</c> providing simple adding of Nodes and attributes
        /// to XmlDocument.
        /// </summary>
        private class XmlDocumentCreator
        {
            /// <summary>
            /// Underlaying <c>XmlDocument</c>.
            /// </summary>
            private readonly XmlDocument document;

            /// <summary>
            /// Construct new instance of <see cref="XmlDocumentCreator"/>
            /// </summary>
            /// <param name="root">Name of the root element.</param>
            public XmlDocumentCreator(string root)
            {
                document = new XmlDocument();
                XmlDeclaration declaration = document.CreateXmlDeclaration("1.0", "UTF-8", null);
                document.InsertBefore(declaration, document.DocumentElement);
                document.AppendChild(document.CreateElement(root));
                using (FileStream str = new FileStream(XsdSchema, FileMode.Open))
                {
                    document.Schemas.Add(XmlSchema.Read(str, null));
                }
            }

            /// <summary>
            /// Appends node as last child element of the root node.
            /// </summary>
            /// <param name="name">Name of the node.</param>
            /// <param name="value">Value of the node, or null it it should have no value.</param>
            /// <returns>Added node.</returns>
            public XmlNode AddNode(string name, string value = null)
            {
                return AddNode(document.DocumentElement, name, value);
            }

            /// <summary>
            /// Appends node as last child element of <see cref="parrent"/> node.
            /// </summary>
            /// <param name="parrent">Parrent node.</param>
            /// <param name="name">Name of the node.</param>
            /// <param name="value">Value of the node, or null it it should have no value.</param>
            /// <returns></returns>
            public XmlNode AddNode(XmlNode parrent, string name, string value)
            {
                XmlNode node = document.CreateElement(name);
                if (value != null)
                    node.InnerText = value;
                parrent.AppendChild(node);
                return node;
            }

            /// <summary>
            /// Adds attribute to <see cref="element"/>.
            /// </summary>
            /// <param name="element">Element to add attribute to.</param>
            /// <param name="name">Name of the attribute.</param>
            /// <param name="value">Value of the attribute.</param>
            public void AddAttribute(XmlNode element, string name, string value)
            {
                XmlAttribute attribute = document.CreateAttribute(name);
                attribute.InnerText = value;
                element.Attributes?.Append(attribute);
            }

            /// <summary>
            /// Returns underlaying <c>XmlDocument</c>.
            /// </summary>
            /// <returns><see cref="document"/></returns>
            public XmlDocument GetXmlDocument()
            {
                return document;
            }

            /// <summary>
            /// Appends node representing <see cref="TextSettings"/> as last child element of <see cref="parrent"/> node.
            /// </summary>
            /// <param name="parrent">Parrent node.</param>
            /// <param name="name">Name of the node.</param>
            /// <param name="settings">Value of the node.</param>
            public void AddNode(XmlNode parrent, string name, TextSettings settings)
            {
                var node = AddNode(parrent, name, settings.TextColor.ToSettingsString());
                AddAttribute(node, "bold", settings.Bold ? "true" : "false");
                AddAttribute(node, "italic", settings.Italic ? "true" : "false");
            }
        }

        /// <summary>
        /// Returns inner text of node selected by XPath expression in <see cref="document"/>.
        /// </summary>
        /// <param name="document"><c>XmlDocument</c></param>
        /// <param name="xpath">XPath expression.</param>
        /// <returns>Inner text representing the settings.</returns>
        private static string GetSettingsFromXPath(XmlDocument document, string xpath)
        {
            return document.SelectSingleNode(xpath)?.InnerText;
        }
        #endregion XmlSupport

        /// <summary>
        /// Saves the <see cref="settings"/> to a file given by <see cref="Path"/>
        /// </summary>
        /// <param name="settings"><see cref="Settings"/> to save.</param>
        /// <exception cref="SettingsSaveException">If <see cref="settings"/> are not valid.</exception>
        public static void SaveToXml(Settings settings)
        {
            XmlDocumentCreator creator = new XmlDocumentCreator("settings");
            creator.AddNode("name", settings.Name);
            creator.AddNode("host", settings.Host);
            creator.AddNode("user", settings.User);
            creator.AddNode("pass", settings.Pass);
            creator.AddNode("sid", settings.Sid);
            creator.AddNode("port", settings.Port);

            var highlighter = creator.AddNode("highlighter");
            creator.AddNode(highlighter, "keyword_color", settings.KeyWord);
            creator.AddNode(highlighter, "function_color", settings.Function);
            creator.AddNode(highlighter, "comment_color", settings.Comment);
            creator.AddNode(highlighter, "stringliteral_color", settings.StringLiteral);
            creator.AddNode(highlighter, "numberliteral_color", settings.NumberLiteral);

            var history = creator.AddNode("history");
            foreach (string command in settings.CommandsHistory)
                creator.AddNode(history, "command", command);

            var snippets = creator.AddNode("snippets");
            foreach (var query in settings.Snippets)
            {
                var node = creator.AddNode(snippets, "query", query.Value);
                creator.AddAttribute(node, "name", query.Key);
            }

            XmlDocument document = creator.GetXmlDocument();
            try
            {
                document.Validate(null);
            }
            catch (XmlSchemaValidationException e)
            {
                int startIndex = e.Message.IndexOf('\'') + 1;
                int endIndex = e.Message.IndexOf('\'', startIndex);
                throw new SettingsSaveException(Program.Language[LangStrings.SaveSettingsException, e.Message.Substring(startIndex, endIndex - startIndex)], e);
            }
            document.Save(settings.Path);
        }

        /// <summary>
        /// Returns path to the file representing this <see cref="Settings"/>.
        /// </summary>
        public string Path => GetXmlPathFromName(Name);

        /// <summary>
        /// Class representing settings of text in highlighing component Scintilla.NET
        /// It allows to define <c>Color</c>, boldness and italics for the text.
        /// </summary>
        public class TextSettings
        {
            /// <summary>
            /// Constructs new instance of <see cref="TextSettings"/> with given arguments.
            /// </summary>
            /// <param name="bold"></param>
            /// <param name="italic"></param>
            /// <param name="textColor"></param>
            public TextSettings(bool bold, bool italic, Color textColor)
            {
                Bold = bold;
                Italic = italic;
                TextColor = textColor;
            }

            /// <summary>
            /// Factory method to create settings from <c>XmlDocument</c> located using XPath expression.
            /// </summary>
            /// <param name="document"><c>XmlDocument</c> containing settings.</param>
            /// <param name="xpath">XPath expression.</param>
            /// <returns><see cref="TextSettings"/> from the XPath expression.</returns>
            public static TextSettings GetTextSettingsFromXPath(XmlDocument document, string xpath)
            {
                return new TextSettings(
                    bold: GetSettingsFromXPath(document, xpath + "/@bold") == "true",
                    italic: GetSettingsFromXPath(document, xpath + "/@italic") == "true",
                    textColor: ColorExtensions.FromString(GetSettingsFromXPath(document, xpath))
                    );
            }

            /// <summary>
            /// Boldness tag.
            /// </summary>
            public bool Bold { get; }

            /// <summary>
            /// Italics tag.
            /// </summary>
            public bool Italic { get; }

            /// <summary>
            /// <c>Color</c> of the text.
            /// </summary>
            public Color TextColor { get; }
        }

        #region SettingsItems
        /// <summary>
        /// Name of the profile.
        /// </summary>
        public string Name { get; set; } = "";

        /// <summary>
        /// Hostname to be used while connecting to database.
        /// </summary>
        public string Host { get; set; } = "";

        /// <summary>
        /// Username to be used while connecting to database.
        /// </summary>
        public string User { get; set; } = "";

        /// <summary>
        /// Username to be used while connecting to database.
        /// </summary>
        public string Pass { get; set; } = "";

        /// <summary>
        /// SID to be used while connecting to database.
        /// </summary>
        public string Sid { get; set; } = "";

        /// <summary>
        /// Port to be used while connecting to database.
        /// </summary>
        public string Port { get; set; } = "1521";

        /// <summary>
        /// <see cref="TextSettings"/>used for SQL keywords.
        /// </summary>
        public TextSettings KeyWord { get; private set; } = new TextSettings(true, false, ColorExtensions.FromString("purple"));

        /// <summary>
        /// <see cref="TextSettings"/>used for function calls.
        /// </summary>
        public TextSettings Function { get; private set; } = new TextSettings(false, false, ColorExtensions.FromString("#569CD6"));

        /// <summary>
        /// <see cref="TextSettings"/>used for comments.
        /// </summary>
        public TextSettings Comment { get; private set; } = new TextSettings(false, false, ColorExtensions.FromString("#57A64A"));

        /// <summary>
        /// <see cref="TextSettings"/>used for string literals.
        /// </summary>
        public TextSettings StringLiteral { get; private set; } = new TextSettings(false, false, ColorExtensions.FromString("red"));

        /// <summary>
        /// <see cref="TextSettings"/> used for number literals.
        /// </summary>
        public TextSettings NumberLiteral { get; private set; } = new TextSettings(false, false, ColorExtensions.FromString("blue"));

        /// <summary>
        /// History of previously executed commands.
        /// </summary>
        public CommandsHistory CommandsHistory = new CommandsHistory(MaxCommandHistory);

        /// <summary>
        /// Snippets stored in the profile.
        /// </summary>
        public IDictionary<string, string> Snippets = new Dictionary<string, string>();
        #endregion

        #region SettingsConstants
        /// <summary>
        /// Gets path to XML file of settings from the profile name.
        /// </summary>
        /// <param name="name">Name of the profile.</param>
        /// <returns>Text containing path to file of settings.</returns>
        public static string GetXmlPathFromName(string name) => SettingsPath + name + XmlExtension;

        /// <summary>
        /// Directory containing settings.
        /// </summary>
        public const string SettingsPath = "settings/";

        /// <summary>
        /// XML Schema file used to validate XML documents with setings.
        /// </summary>
        public const string XsdSchema = "settings.xsd";

        /// <summary>
        /// Extension used for XML files.
        /// </summary>
        public const string XmlExtension = ".xml";
        #endregion SettingsConstants

        /// <summary>
        /// XML Schema once it is loaded in <see cref="LoadFromXml"/>.
        /// </summary>
        private static XmlSchema schema;

        /// <summary>
        /// Adds executed command to the history.
        /// </summary>
        /// <param name="command">Command to add to history.</param>
        public void AddExecutedCommand(string command)
        {
            CommandsHistory.Add(command);
        }

        /// <summary>
        /// Saves query snippet to the profile.
        /// </summary>
        /// <param name="name">Name of the snippet.</param>
        /// <param name="query">Actuall query.</param>
        public void SaveQuery(string name, string query)
        {
            if (!Snippets.ContainsKey(name))
                Snippets.Add(name, query);
            else
                MessageBox.Show(Program.Language[LangStrings.QueryAlreadyExists, name], Program.Language[LangStrings.Error]);
        }
    }
}
