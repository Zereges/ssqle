﻿using System;
using System.Collections.Generic;

namespace SSQLE
{
    /// <summary>
    /// Localization strings enumeration.
    /// </summary>
    public enum LangStrings
    {
        Error,
        XmlLoadExceptionWellFormed,
        XmlLoadExceptionValid,
        NewUser,
        UpdateExistingUser,
        OverrideExistingUser,
        SaveSettingsException,
        InvalidConnectionName,
        InvalidConnectionHostname,
        InvalidConnectionSid,
        InvalidConnectionPort,
        ConnectionFailed,
        QueryNotExecuted,
        Elapsed,
        RecordsAffected,
        ExecutingQuery,
        SaveFile,
        CommaSeparatedValuesFilter,
        SqlFileFilter,
        FileSaved,
        DeleteUserProfile,
        CantSaveSettingsOnExit,
        SaveQueryInvalidIdentifier,
        QueryAlreadyExists,
        QueryNotFound,
    }

    /// <summary>
    /// Iterface providing facilities for easier translation of the application to different language.
    /// </summary>
    /// <seealso cref="EnglishLang"/>
    public interface ILang
    {
        /// <summary>
        /// Tries to get textual representation of the <see cref="LangStrings"/>.
        /// </summary>
        /// <param name="index"><see cref="LangStrings"/> which textual representation we want.</param>
        /// <param name="value"><c>out</c> value where to store textual representation of <paramref name="index"/>.</param>
        /// <returns>True if textual representation was provided, false otherwise.</returns>
        bool TryGetValue(LangStrings index, out string value);
    }

    /// <summary>
    /// Implements textual strings in english language for this application.
    /// Does not provide strings for hardcoded items in GUI.
    /// </summary>
    internal class EnglishLang : ILang
    {
        /// <summary>
        /// <seealso cref="ILang.TryGetValue"/>.
        /// </summary>
        /// <param name="index"><see cref="LangStrings"/> which textual representation we want.</param>
        /// <param name="value"><c>out</c> value where to store textual representation of <paramref name="index"/>.</param>
        /// <returns>True if textual representation was provided, false otherwise.</returns>
        public bool TryGetValue(LangStrings index, out string value)
        {
            return lang.TryGetValue(index, out value);
        }

        /// <summary>
        /// Mapping of <see cref="LangStrings"/> to textual representations.
        /// Mapped value might contain <c>{0}</c>, <c>{1}</c>, ... values to be replaced.
        /// </summary>
        private readonly Dictionary<LangStrings, string> lang = new Dictionary<LangStrings, string>()
        {
            { LangStrings.Error, "Error" },
            { LangStrings.XmlLoadExceptionWellFormed, "Failed to load XML. XML is not well formed." },
            { LangStrings.XmlLoadExceptionValid, "Failed to load XML. XML can't be validated." },
            { LangStrings.NewUser, "<New user>" },
            { LangStrings.UpdateExistingUser, "Are you sure you want to modify the user?" },
            { LangStrings.OverrideExistingUser, "User {0} already exists. Override?" },
            { LangStrings.SaveSettingsException, "Settings can not be saved. Value in '{0}' is invalid." },
            { LangStrings.InvalidConnectionName, "Connection name is not valid." },
            { LangStrings.InvalidConnectionHostname, "Connection host name is not valid." },
            { LangStrings.InvalidConnectionSid, "Connection SID is not valid." },
            { LangStrings.InvalidConnectionPort, "Connection port is not valid." },
            { LangStrings.ConnectionFailed, "Can't connect to server: {0}" },
            { LangStrings.QueryNotExecuted, "Can't execute the query: {0}" },
            { LangStrings.Elapsed, "Elapsed: {0} sec." },
            { LangStrings.RecordsAffected, "Records affected: {0}" },
            { LangStrings.ExecutingQuery, "Executing the query." },
            { LangStrings.SaveFile, "Save file" },
            { LangStrings.CommaSeparatedValuesFilter, "Comma separated values|*.csv" },
            { LangStrings.SqlFileFilter, "SQL File|*.sql" },
            { LangStrings.FileSaved, "File {0} saved." },
            { LangStrings.DeleteUserProfile, "Are you sure you want to delete profile {0}." },
            { LangStrings.CantSaveSettingsOnExit, "Settings could not be saved. Do you still want to close?\n{0}" },
            { LangStrings.SaveQueryInvalidIdentifier, "That is not valid name as query identifier." },
            { LangStrings.QueryAlreadyExists, "Query {0} already exists." },
            { LangStrings.QueryNotFound, "Query {0} not found." },
        };
    }

    /// <summary>
    /// Singleton class used to handle retrieval of <see cref="LangStrings"/> from <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Implementation of <see cref="ILang"/>.</typeparam>
    public sealed class Lang<T> where T : ILang, new()
    {
        /// <summary>
        /// Singleton constructor.
        /// </summary>
        private Lang()
        {
        }

        /// <summary>
        /// Indexer for textual representations of <see cref="LangStrings"/>.
        /// </summary>
        /// <param name="index"><see cref="LangStrings"/> which textual representation we want.</param>
        /// <param name="values">optional array of <c>object</c> to provide replacements for placeholders in strings.</param>
        /// <returns>Textual representation of <paramref name="index"/> with placeholders replaced with <paramref name="values"/>.</returns>
        /// <exception cref="NotImplementedException">If <see cref="ILang.TryGetValue"/> fails.</exception>
        public string this[LangStrings index, params object[] values]
        {
            get
            {
                string str;
                if (lang.TryGetValue(index, out str))
                    return string.Format(str, values);
                throw new NotImplementedException("Not implemented text value of LangStrings index " + index);
            }
        }

        /// <summary>
        /// Implementation of language.
        /// </summary>
        private readonly T lang = new T();

        /// <summary>
        /// Singleton instance.
        /// </summary>
        public static Lang<T> Instance { get; private set; } = new Lang<T>();
    }
}
