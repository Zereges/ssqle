using System;
using System.Drawing;

namespace SSQLE
{
    /// <summary>
    /// Extension and support methods for manipulating with <c>Color</c> structure.
    /// </summary>
    internal static class ColorExtensions
    {
        /// <summary>
        /// Converts a <c>Color</c> to hexa code to be used in settings.
        /// </summary>
        /// <param name="color">Color to convert.</param>
        /// <returns>Hexa code in format \#RRGGBB, where R,G,B are hexadecimal numbers.</returns>
        private static string ToHexaCode(this Color color)
        {
            return $"#{color.R.ToString("x2")}{color.G.ToString("x2")}{color.B.ToString("x2")}";
        }

        /// <summary>
        /// Converts a string to a <c>Color</c> based on its content.
        /// </summary>
        /// <param name="str">String to convert.</param>
        /// <returns><c>Color</c> structure constructed from <paramref name="str"/>.</returns>
        /// <remarks>
        /// Is capable of converting strings in two formats, either
        ///     - \#RRGGBB ; As HexaCode. <seealso cref="ToHexaCode"/>.
        ///     - string, for which Color.FromName(string) returns valid color. 
        /// </remarks>
        /// <exception cref="ArgumentException">If invalid string is provided.</exception>
        public static Color FromString(string str)
        {
            str = str.Trim();
            try
            {
                if (str[0] == '#')
                {
                    if (str.Length < 7)
                        throw new ArgumentException("Provided string is too short (Expecting at least 7 characters)");

                    return Color.FromArgb(
                        byte.Parse(str.Substring(1, 2), System.Globalization.NumberStyles.HexNumber),
                        byte.Parse(str.Substring(3, 2), System.Globalization.NumberStyles.HexNumber),
                        byte.Parse(str.Substring(5, 2), System.Globalization.NumberStyles.HexNumber)
                    );
                }
                return Color.FromName(str);
            }
            catch (Exception e)
            {
                throw new ArgumentException($"Can not convert {str} to color. Expecting #RRGGBB or textual representation.", e);
            }
        }

        /// <summary>
        /// Converts <paramref name="color"/> to it's name. Only Colors, which are members of <c>KnownColor</c> enumeration
        /// are valid.
        /// </summary>
        /// <param name="color">Color, which name we want to know.</param>
        /// <returns>
        /// Name of the color, or <c>null</c> if <paramref name="color"/> is not member of <c>KnownColor</c> enumeration
        /// </returns>
        private static string ToName(this Color color)
        {
            if (color.IsNamedColor)
            {
                string str = color.ToString();
                int index = str.IndexOf('[') + 1;
                return str.Substring(index, str.IndexOf(']') - index);
            }
            return null;
        }

        /// <summary>
        /// Converts <paramref name="color"/> to string either using <see cref="ToName"/> or <see cref="ToHexaCode"/>.
        /// </summary>
        /// <param name="color"><c>Color</c> to convert.</param>
        /// <returns>Textual representation to be used in Settings.</returns>
        /// <seealso cref="Settings"/>
        public static string ToSettingsString(this Color color)
        {
            return color.ToName() ?? color.ToHexaCode();
        }
    }
}
