﻿namespace SSQLE.Forms
{
    partial class QueryResultWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scintilla_queryresult = new ScintillaNET.Scintilla();
            this.SuspendLayout();
            // 
            // scintilla_queryresult
            // 
            this.scintilla_queryresult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scintilla_queryresult.Location = new System.Drawing.Point(0, 0);
            this.scintilla_queryresult.Name = "scintilla_queryresult";
            this.scintilla_queryresult.Size = new System.Drawing.Size(284, 262);
            this.scintilla_queryresult.TabIndex = 0;
            // 
            // QueryResultWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.scintilla_queryresult);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "QueryResultWindow";
            this.Text = "Query result";
            this.ResumeLayout(false);

        }

        #endregion

        private ScintillaNET.Scintilla scintilla_queryresult;
    }
}