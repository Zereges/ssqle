﻿using System;
using System.IO;
using System.Windows.Forms;
using ScintillaNET;
using SSQLE.Forms;

namespace SSQLE
{
    /// <summary>
    /// Class representing an SQL Editor with input, output and other features.
    /// </summary>
    public partial class Editor : Form
    {
        /// <summary>
        /// Connection that the editor uses for executing queries.
        /// </summary>
        private readonly IConnection connection;

        /// <summary>
        /// Result of last executed query.
        /// </summary>
        private QueryResult currentQueryResult;

        /// <summary>
        /// Index to history commands. See <see cref="CommandsHistory"/>.
        /// </summary>
        private int historyIndex;

        /// <summary>
        /// Constructs new editor with given <see cref="IConnection"/>.
        /// </summary>
        /// <param name="connection">Connection, which Editor uses for executing queries.</param>
        public Editor(IConnection connection)
        {
            InitializeComponent();
            ConfigureScintillaTextSettings();
            ConfigureScintillaKeywords();
            this.connection = connection;
            historyIndex = Program.Settings.CommandsHistory.Count;
            if (Program.Settings.CommandsHistory.Count == 0)
                btn_previous_cmd.Enabled = false;

            foreach (var snippet in Program.Settings.Snippets)
            {
                AddLoadQueryDropDown(snippet.Key);
            }
            if (Program.Settings.Snippets.Count == 0)
                loadQueryToolStripMenuItem.DropDownItems.Add(new ToolStripButton()
                {
                    Text = @"<empty>",
                    Enabled = false
                });

            Closing += (sender, args) => { connection.Close(); };
        }

        /// <summary>
        /// Configures Scintilla.NET settings, which can't be configured in designer.
        /// </summary>
        private void ConfigureScintillaTextSettings()
        {
            scintillaInput.Margins[1].Width = 0; // Left indicator box.
            scintillaResults.Margins[1].Width = 0;
            scintillaResults.Styles[Style.Default].Font = "Courier New";
            scintillaResults.Styles[Style.Default].Size = 10;

            scintillaInput.StyleResetDefault();
            scintillaInput.Styles[Style.Default].Font = "Courier New";
            scintillaInput.Styles[Style.Default].Size = 10;
            scintillaInput.StyleClearAll();

            scintillaInput.Styles[Style.Sql.CommentLine].ForeColor = Program.Settings.Comment.TextColor;
            scintillaInput.Styles[Style.Sql.CommentLine].Bold = Program.Settings.Comment.Bold;
            scintillaInput.Styles[Style.Sql.CommentLine].Italic = Program.Settings.Comment.Italic;
            scintillaInput.Styles[Style.Sql.Comment].ForeColor = Program.Settings.Comment.TextColor;
            scintillaInput.Styles[Style.Sql.Comment].Bold = Program.Settings.Comment.Bold;
            scintillaInput.Styles[Style.Sql.Comment].Italic = Program.Settings.Comment.Italic;
            scintillaInput.Styles[Style.Sql.Character].ForeColor = Program.Settings.StringLiteral.TextColor;
            scintillaInput.Styles[Style.Sql.Character].Bold = Program.Settings.StringLiteral.Bold;
            scintillaInput.Styles[Style.Sql.Character].Italic = Program.Settings.StringLiteral.Italic;
            scintillaInput.Styles[Style.Sql.Number].ForeColor = Program.Settings.NumberLiteral.TextColor;
            scintillaInput.Styles[Style.Sql.Number].Bold = Program.Settings.NumberLiteral.Bold;
            scintillaInput.Styles[Style.Sql.Number].Italic = Program.Settings.NumberLiteral.Italic;
            scintillaInput.Styles[Style.Sql.Word].ForeColor = Program.Settings.KeyWord.TextColor;
            scintillaInput.Styles[Style.Sql.Word].Bold = Program.Settings.KeyWord.Bold;
            scintillaInput.Styles[Style.Sql.Word].Italic = Program.Settings.KeyWord.Italic;
            scintillaInput.Styles[Style.Sql.Word2].ForeColor = Program.Settings.Function.TextColor;
            scintillaInput.Styles[Style.Sql.Word2].Bold = Program.Settings.Function.Bold;
            scintillaInput.Styles[Style.Sql.Word2].Italic = Program.Settings.Function.Italic;
            scintillaInput.Lexer = Lexer.Sql;
        }

        /// <summary>
        /// Configures Scintilla.NET settings, which can't be configured in designer.
        /// </summary>
        private void ConfigureScintillaKeywords()
        {
            scintillaInput.SetKeywords(0,
                "access add all alter and any as asc audit between by char check cluster column comment compress connect create current date" +
                " decimal default delete desc distinct drop else exclusive exists file float for from grant group having identified immediate in" +
                " increment index initial insert integer intersect into is level like lock long maxextents minus mlslabel mode modify noaudit" +
                " nocompress not nowait null number of offline on online option or order pctfree prior privileges public raw rename resource" +
                " revoke row rowid rownum rows select session set share size smallint start successful synonym sysdate table then to trigger" +
                " uid union unique update user validate values varchar varchar2 view whenever where with");
            scintillaInput.SetKeywords(1,
                "abs mod power round trunc sin cos tan asin acos atan sinh cosh tanh sqrt exp ln log ceil floor sign initcap lower upper concat" +
                " lpad rpad ltrim rtrim replace substr length instr nanvl nvl isnull ifnull coalesce to_char to_number avg count count max min sum" +
                " corr median stddev variance add_months last_day months_between new_time next_day sysdate greatest least to_char to_date round trunc");
        }

        /// <summary>
        /// Handler, that is executed whenever user clicks on Execute button, or presses key command to execute query.
        /// </summary>
        /// <param name="sender"><c>Button</c>, that was clicked on, or null if it was executed by key stroke.</param>
        /// <param name="e">Event arguments.</param>
        private void btn_execute_Click(object sender, EventArgs e)
        {
            string queryText = scintillaInput.SelectedText;
            if (string.IsNullOrEmpty(queryText))
                queryText = scintillaInput.Text;
            queryText = queryText.TrimEnd();

            if (queryText.Length == 0)
                return;

            if (queryText[queryText.Length - 1] == ';')
                queryText = queryText.Substring(0, queryText.Length - 1);

            SetStatusBarText(Program.Language[LangStrings.ExecutingQuery]); // ToDo: not working, do execute in new thread??
            currentQueryResult = connection.ExecuteQuery(queryText);
            if (currentQueryResult != null)
            {
                // Scintilla.NET does not allow even itself to change it's text if ReadOnly == true
                scintillaResults.ReadOnly = false;
                if (scintillaResults.Text.Length != 0)
                    scintillaResults.Text += "\n\n\n";

                scintillaResults.Text += currentQueryResult.ToString();
                scintillaResults.ReadOnly = true;
                SetStatusBarText(Program.Language[LangStrings.Elapsed, currentQueryResult.ElapsedTime]);
                Program.Settings.AddExecutedCommand(scintillaInput.Text);
                historyIndex = Program.Settings.CommandsHistory.Count;
                btn_next_cmd.Enabled = false;
                btn_previous_cmd.Enabled = true;
            }
            else
            {
                currentQueryResult = null;
                SetStatusBarText();
            }
        }

        /// <summary>
        /// Changes text of status bar.
        /// </summary>
        /// <param name="newText">New text of the status bar.</param>
        private void SetStatusBarText(string newText = "")
        {
            statusBar.Items[0].Text = newText;
        }

        /// <summary>
        /// Handler, that is executed whenever user clicks on Save query as CSV.
        /// </summary>
        /// <param name="sender"><c>ToolStripMenuItem</c>, that was clicked on.</param>
        /// <param name="e">Event arguments.</param>
        private void asCsvToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentQueryResult == null)
                return;

            string fileName = SaveFileDialog(Program.Language[LangStrings.CommaSeparatedValuesFilter], currentQueryResult.SaveToCsv);
            if (fileName != string.Empty)
                SetStatusBarText(Program.Language[LangStrings.FileSaved, fileName]);
        }

        /// <summary>
        /// Handler, that is executed whenever user clicks on Save query as SQL INSERT statement.
        /// </summary>
        /// <param name="sender"><c>ToolStripMenuItem</c>, that was clicked on.</param>
        /// <param name="e">Event arguments.</param>
        private void asInsertStatementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentQueryResult == null)
                return;

            string fileName = SaveFileDialog(Program.Language[LangStrings.SqlFileFilter], currentQueryResult.SaveToInsertStatement);
            if (fileName != string.Empty)
                SetStatusBarText(Program.Language[LangStrings.FileSaved, fileName]);
        }

        /// <summary>
        /// Performs save of query results to format driven by <paramref name="dataProvider"/> to a file using <c>SaveFileDialog</c>.
        /// </summary>
        /// <param name="filter">Filter for files in the dialog.</param>
        /// <param name="dataProvider">Function, that provides the conversion of Query Result.</param>
        /// <returns>Name of the file</returns>
        private static string SaveFileDialog(string filter, Action<TextWriter> dataProvider)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = filter,
                Title = Program.Language[LangStrings.SaveFile]
            };
            if (DialogResult.OK != saveFileDialog.ShowDialog())
                return string.Empty;

            using (TextWriter writer = new StreamWriter(saveFileDialog.OpenFile()))
                dataProvider(writer);
            return saveFileDialog.FileName;
        }


        /// <summary>
        /// Handler, that is executed whenever user clicks on File close menu item.
        /// It closes the <see cref="Editor"/> form.
        /// </summary>
        /// <param name="sender"><c>ToolStripMenuItem</c>, that was clicked on.</param>
        /// <param name="e">Event arguments.</param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Handler, that is executed whenever user clicks on next command from history button, or presses key command to execute query.
        /// </summary>
        /// <param name="sender"><c>Button</c>, that was clicked on, or null if it was executed by key stroke.</param>
        /// <param name="e">Event arguments.</param>
        private void btn_next_cmd_Click(object sender, EventArgs e)
        {
            if (++historyIndex >= Program.Settings.CommandsHistory.Count)
            {
                historyIndex = Program.Settings.CommandsHistory.Count;
                SetCommandFromHistory("");
                btn_next_cmd.Enabled = false;
            }
            else
            {
                SetCommandFromHistory(Program.Settings.CommandsHistory[historyIndex]);
                btn_previous_cmd.Enabled = true;
            }
        }

        /// <summary>
        /// Handler, that is executed whenever user clicks on previous command from history button, or presses key command to execute query.
        /// </summary>
        /// <param name="sender"><c>Button</c>, that was clicked on, or null if it was executed by key stroke.</param>
        /// <param name="e">Event arguments.</param>
        private void btn_previous_cmd_Click(object sender, EventArgs e)
        {
            if (--historyIndex < 0 || Program.Settings.CommandsHistory.Count == 0)
            {
                historyIndex = -1;
                SetCommandFromHistory("");
                btn_previous_cmd.Enabled = false;
            }
            else
            {
                SetCommandFromHistory(Program.Settings.CommandsHistory[historyIndex]);
                btn_next_cmd.Enabled = true;
            }
        }

        /// <summary>
        /// Changes the text of Query editor to given argument.
        /// </summary>
        /// <param name="command">Command to set the query editor to.</param>
        private void SetCommandFromHistory(string command)
        {
            scintillaInput.Text = command;
        }

        /// <summary>
        /// Handler, that is called when <see cref="Editor"/> is closing.
        /// Handles <see cref="SettingsSaveException"/>.
        /// </summary>
        /// <param name="sender"><see cref="Editor"/></param>
        /// <param name="e">Form closing event arguments.</param>
        private void Editor_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Settings.SaveToXml(Program.Settings);
            }
            catch (SettingsSaveException ex)
            {
                if (DialogResult.No == MessageBox.Show(Program.Language[LangStrings.CantSaveSettingsOnExit, ex.Message], Program.Language[LangStrings.Error], MessageBoxButtons.YesNo))
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Handler, that is executed whenever user clicks on Save query menu item.
        /// </summary>
        /// <param name="sender"><c>ToolStripMenuItem</c>, that was clicked on.</param>
        /// <param name="e">Event arguments.</param>
        private void queryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveQueryDialog dialog = new SaveQueryDialog(this);
            dialog.ShowDialog();
        }

        /// <summary>
        /// Saves the query currently in query editor by given name to <see cref="Settings"/>.
        /// </summary>
        /// <param name="name">Identifier of the query for further reuse.</param>
        public void SaveQuery(string name)
        {
            if (Program.Settings.Snippets.Count == 0)
                loadQueryToolStripMenuItem.DropDownItems.Clear(); // remove <empty> indicator

            Program.Settings.SaveQuery(name, scintillaInput.Text);
            AddLoadQueryDropDown(name);
        }

        /// <summary>
        /// Adds menu item to Load menu for loading a saved query.
        /// </summary>
        /// <param name="name">Name of the query.</param>
        private void AddLoadQueryDropDown(string name)
        {
            var menuItem = new ToolStripMenuItem(name);
            menuItem.Click += btn_load_query_Click;
            loadQueryToolStripMenuItem.DropDownItems.Add(menuItem);
        }

        /// <summary>
        /// Handler, that is executed whenever user clicks on load query menu item.
        /// </summary>
        /// <param name="sender"><c>ToolStripMenuItem</c>, that was clicked on.</param>
        /// <param name="e">Event arguments.</param>
        private void btn_load_query_Click(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem) sender;
            string query;
            if (Program.Settings.Snippets.TryGetValue(menuItem.Text, out query))
            {
                scintillaInput.Text = query;
                scintillaInput.SelectionStart = scintillaInput.TextLength;
            }
            else
                MessageBox.Show(Program.Language[LangStrings.QueryNotFound, menuItem.Text], Program.Language[LangStrings.Error]);
        }

        /// <summary>
        /// Handler, that is executed whenever user presses key.
        /// </summary>
        /// <param name="sender"><see cref="Editor"/>.</param>
        /// <param name="e">Key event arguments.</param>
        private void Editor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Control)
            {
                e.SuppressKeyPress = true;
                btn_execute_Click(null, null);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                e.SuppressKeyPress = true;
                btn_previous_cmd_Click(null, null);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                e.SuppressKeyPress = true;
                btn_next_cmd_Click(null, null);
            }
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            scintillaResults.ReadOnly = false;
            scintillaResults.Text = "";
            scintillaResults.ReadOnly = true;
        }

        private void btn_extractquery_Click(object sender, EventArgs e)
        {
            Program.ApplicationContext.ShowForm(new QueryResultWindow(currentQueryResult));
        }
    }
}
