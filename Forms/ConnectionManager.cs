﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace SSQLE
{
    /// <summary>
    /// Class representing a manager for connections in SQL editor.
    /// </summary>
    public partial class ConnectionManager : Form
    {
        /// <summary>
        /// Pair of profile name and profile path to be used in connections frop down list.
        /// </summary>
        private class ProfilePathPair
        {
            /// <summary>
            /// Constructs new pair of profile and path.
            /// </summary>
            /// <param name="profile">Profile name.</param>
            /// <param name="path">Profile path.</param>
            public ProfilePathPair(string profile, string path)
            {
                Profile = profile;
                Path = path;
            }

            /// <summary>
            /// Returns textual representation of the pair.
            /// </summary>
            /// <returns>Profile name/</returns>
            public override string ToString()
            {
                return Profile;
            }

            /// <summary>
            /// Path to the profile.
            /// </summary>
            public string Path { get; }

            /// <summary>
            /// Name of the profile.
            /// </summary>
            public string Profile { get; }
        }

        /// <summary>
        /// Constructs new <see cref="ConnectionManager"/>.
        /// </summary>
        public ConnectionManager()
        {
            InitializeComponent();
            Directory.CreateDirectory(Settings.SettingsPath);
            UpdateComboBoxItems();
            cm_profiles.SelectedIndex = 0;
        }

        /// <summary>
        /// Updates items in drop down <c>ComboBox</c>. Is called when connection profile is modified or deleted.
        /// </summary>
        private void UpdateComboBoxItems()
        {
            cm_profiles.Items.Clear();
            DirectoryInfo info = new DirectoryInfo(Settings.SettingsPath);
            FileInfo[] files = info.GetFiles();

            // Sort by creation-time descending 
            Array.Sort(files, (f1, f2) => f2.CreationTime.CompareTo(f1.CreationTime));

            foreach (FileInfo file in files)
            {
                cm_profiles.Items.Add(new ProfilePathPair(Path.GetFileNameWithoutExtension(file.Name), Settings.SettingsPath + file.Name));
            }
            cm_profiles.Items.Add(new ProfilePathPair(Program.Language[LangStrings.NewUser], null));
        }

        /// <summary>
        /// Handler, that is called whenever drop down <c>ComboBox</c> selected index changes.
        /// </summary>
        /// <param name="sender"><c>ComboBox</c> that triggered this event.</param>
        /// <param name="e">Event arguments.</param>
        private void cm_profiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox) sender;
            ProfilePathPair selected = (ProfilePathPair) comboBox.SelectedItem;

            if (selected.Path != null)
            {
                try
                {
                    using (FileStream xml = new FileStream(selected.Path, FileMode.Open))
                    {
                        Program.LoadSettings(xml);
                    }
                }
                catch (SettingsLoadException ex)
                {
                    MessageBox.Show(ex.ToString(), Program.Language[LangStrings.Error]);
                }
            }
            else
                Program.LoadSettings(null);
            UpdateTextComponents();
        }

        /// <summary>
        /// Updates text <c>TextBox</c> with values from <see cref="Settings"/>.
        /// </summary>
        private void UpdateTextComponents()
        {
            cm_name.Text = Program.Settings.Name;
            cm_host.Text = Program.Settings.Host;
            cm_user.Text = Program.Settings.User;
            cm_pass.Text = Program.Crypto.Decrypt(Program.Settings.Pass);
            cm_sid.Text = Program.Settings.Sid;
            cm_port.Text = Program.Settings.Port;
        }

        /// <summary>
        /// Handler, that is called when user clicks Save button.
        /// </summary>
        /// <param name="sender"><see cref="Button"/> that triggered the event.</param>
        /// <param name="e">Event arguments.</param>
        private void cm_save_Click(object sender, EventArgs e)
        {
            if (cm_profiles.SelectedText != Program.Language[LangStrings.NewUser]) // Update previous user
            {
                if (DialogResult.Yes != MessageBox.Show(Program.Language[LangStrings.UpdateExistingUser], "", MessageBoxButtons.YesNo))
                    return;
            }
            if (cm_name.Text != Program.Settings.Name && File.Exists(Settings.GetXmlPathFromName(cm_name.Text)))
            {
                if (DialogResult.Yes != MessageBox.Show(Program.Language[LangStrings.OverrideExistingUser, cm_name.Text], "", MessageBoxButtons.YesNo))
                    return;
            }

            if (!ValidateInput())
                return;

            SaveToSettings();
            try
            {
                Settings.SaveToXml(Program.Settings);
            }
            catch (SettingsSaveException ex)
            {
                MessageBox.Show(ex.Message, Program.Language[LangStrings.Error]);
                return;
            }
            UpdateComboBoxItems();
            UpdateComboBoxSelectedIndex();
        }

        /// <summary>
        /// Changes selected drop down <c>ComboBox</c> inded when connection is modified, so that
        /// it still points to the same connection profile.
        /// </summary>
        private void UpdateComboBoxSelectedIndex()
        {
            for (int i = 0; i < cm_profiles.Items.Count; ++i)
                if (cm_profiles.Items[i].ToString() == Program.Settings.Name)
                {
                    cm_profiles.SelectedIndex = i;
                    break;
                }
        }

        /// <summary>
        /// Saves current values to settings and encrypts the password.
        /// </summary>
        private void SaveToSettings()
        {
            Program.Settings.Name = cm_name.Text;
            Program.Settings.Host = cm_host.Text;
            Program.Settings.User = cm_user.Text;
            Program.Settings.Pass = Program.Crypto.Encrypt(cm_pass.Text);
            Program.Settings.Sid = cm_sid.Text;
            Program.Settings.Port = cm_port.Text;
        }

        /// <summary>
        /// Handler, that is called when user clicks Delete button.
        /// </summary>
        /// <param name="sender"><see cref="Button"/> that triggered the event.</param>
        /// <param name="e">Event arguments.</param>
        private void cm_delete_Click(object sender, EventArgs e)
        {
            ProfilePathPair selected = (ProfilePathPair) cm_profiles.SelectedItem;
            if (selected != null && DialogResult.Yes == MessageBox.Show(Program.Language[LangStrings.DeleteUserProfile, selected.Path], "", MessageBoxButtons.YesNo))
            {
                File.Delete(selected.Path);
                UpdateComboBoxItems();
                cm_profiles.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Handler, that is called when user clicks Connect button.
        /// It validates connection parameters and takes care of closing this, and showing <see cref="Editor"/> form.
        /// </summary>
        /// <param name="sender"><see cref="Button"/> that triggered the event.</param>
        /// <param name="e">Event arguments.</param>
        private void cm_connect_Click(object sender, EventArgs e)
        {
            if (!ValidateInput(false)) // Shows potential errors.
                return;
            SaveToSettings();

            IConnection connection = OracleConnection.NewConnection(
                host: Program.Settings.Host,
                port: Program.Settings.Port,
                sid: Program.Settings.Sid,
                user: Program.Settings.User,
                pass: Program.Settings.Pass
            );

            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Program.Language[LangStrings.ConnectionFailed, ex.Message], Program.Language[LangStrings.Error]);
                return;
            }
            Program.ApplicationContext.ShowForm(new Editor(connection), true);
            Close();
        }

        /// <summary>
        /// Validates input the user entered into <c>TextBox</c>es.
        /// Validation of the name is not required when arguments are used to connect, but not to save the connection.
        /// </summary>
        /// <param name="validateName">Whether validation of the name is required.</param>
        /// <returns>True iff all arguments are valid according to their rules.</returns>
        /// <seealso cref="ValidateTextBoxForIdentifier"/>
        /// <seealso cref="ValidateTextBoxForHostname"/>
        /// <seealso cref="ValidateTextBoxForSid"/>
        /// <seealso cref="ValidateTextBoxForPort"/>
        private bool ValidateInput(bool validateName = true)
        {
            if (validateName && !ValidateTextBoxForIdentifier(cm_name))
            {
                MessageBox.Show(Program.Language[LangStrings.InvalidConnectionName], "", MessageBoxButtons.OK);
                return false;
            }
            if (!ValidateTextBoxForHostname(cm_host))
            {
                MessageBox.Show(Program.Language[LangStrings.InvalidConnectionHostname], "", MessageBoxButtons.OK);
                return false;
            }
            if (!ValidateTextBoxForSid(cm_sid))
            {
                MessageBox.Show(Program.Language[LangStrings.InvalidConnectionSid], "", MessageBoxButtons.OK);
                return false;
            }
            if (!ValidateTextBoxForPort(cm_port))
            {
                MessageBox.Show(Program.Language[LangStrings.InvalidConnectionPort], "", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Minimal value for port argument.
        /// </summary>
        private const int PortMin = 0;

        /// <summary>
        /// Maximal value for port argument.
        /// </summary>
        private const int PortMax = 65535;

        /// <summary>
        /// Regular expression pattern used to validate identifier. Identifier has to be at least one character long
        /// and must contain only alpha numeric characters and underscore.
        /// </summary>
        private static readonly Regex IdentifierPattern = new Regex("[a-zA-Z0-9_]+");

        /// <summary>
        /// Regular expression pattern used to validate hostname. Matches hostname, IPv4 or IPV6 address
        /// </summary>
        /// <remarks>
        /// It is not perfect, but it is sufficent to reject nonsense.
        /// </remarks>
        private static readonly Regex HostnamePattern = new Regex("([a-zA-Z_0-9-]+)((\\.|::?)[a-zA-Z_0-9-]+)*"); // simple hostname, IPv4, IPv6

        /// <summary>
        /// Checks whether given <paramref name="textBox"/> text contains valid identifier.
        /// </summary>
        /// <param name="textBox"><c>TextBox</c> to check</param>
        /// <returns>True if text is valid, false otherwise.</returns>
        /// <see cref="IdentifierPattern"/>
        public static bool ValidateTextBoxForIdentifier(TextBox textBox)
        {
            return IdentifierPattern.Match(textBox.Text).Success;
        }

        /// <summary>
        /// Checks whether given <paramref name="textBox"/> text contains valid SID.
        /// Decays to <see cref="ValidateTextBoxForIdentifier"/>.
        /// </summary>
        /// <param name="textBox"><c>TextBox</c> to check</param>
        /// <returns>True if text is valid, false otherwise.</returns>
        private static bool ValidateTextBoxForSid(TextBox textBox)
        {
            return ValidateTextBoxForIdentifier(textBox);
        }

        /// <summary>
        /// Checks whether given <paramref name="textBox"/> text contains valid hostname.
        /// </summary>
        /// <param name="textBox"><c>TextBox</c> to check</param>
        /// <returns>True if text is valid, false otherwise.</returns>
        /// <see cref="HostnamePattern"/>
        private static bool ValidateTextBoxForHostname(TextBox textBox)
        {
            return HostnamePattern.Match(textBox.Text).Success;
        }

        /// <summary>
        /// Checks whether given <paramref name="textBox"/> text contains valid port, that is number between
        /// <see cref="PortMin"/> and <see cref="PortMax"/> inclusive.
        /// </summary>
        /// <param name="textBox"><c>TextBox</c> to check</param>
        /// <returns>True if text is valid, false otherwise.</returns>
        /// <see cref="PortMin"/>
        /// <see cref="PortMax"/>
        private static bool ValidateTextBoxForPort(TextBox textBox)
        {
            int value;
            if (!int.TryParse(textBox.Text, out value))
                return false;
            return value >= PortMin && value <= PortMax;
        }
    }
}
