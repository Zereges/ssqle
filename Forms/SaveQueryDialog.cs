﻿using System;
using System.Windows.Forms;

namespace SSQLE
{
    /// <summary>
    /// Simple form representing a dialog window asking the user for name for the query to be saved.
    /// Once shown, it forbids the user to change the focus to it's parrent.
    /// </summary>
    public partial class SaveQueryDialog : Form
    {
        /// <summary>
        /// Reference to <see cref="Editor"/>.
        /// </summary>
        private readonly Editor editor;

        /// <summary>
        /// Constructs new instance of the dialog that is not shown yet.
        /// </summary>
        /// <param name="editor">Reference to <see cref="Editor"/>.</param>
        public SaveQueryDialog(Editor editor)
        {
            InitializeComponent();
            this.editor = editor;
        }

        /// <summary>
        /// Checks, whether text in <paramref name="nameTextBox"/> is valid name.
        /// Decays to <see cref="ConnectionManager.ValidateTextBoxForIdentifier"/>
        /// </summary>
        /// <param name="nameTextBox"><c>TextBox</c> to be validated.</param>
        /// <returns>True, if <paramref name="nameTextBox"/> contains a valid name, False otherwise.</returns>
        private static bool ValidateTextBoxForName(TextBox nameTextBox)
        {
            return ConnectionManager.ValidateTextBoxForIdentifier(nameTextBox);
        }

        /// <summary>
        /// Handler, that is executed whenever user clicks on Save button in the Dialog window.
        /// </summary>
        /// <param name="sender"><c>Button</c>, that was clicked on.</param>
        /// <param name="e">Event arguments.</param>
        private void btn_save_Click(object sender, EventArgs e)
        {
            if (ValidateTextBoxForName(name))
            {
                editor.SaveQuery(name.Text);
                Close();
            }
            else
                MessageBox.Show(Program.Language[LangStrings.SaveQueryInvalidIdentifier]);
        }

        /// <summary>
        /// Handler, that is executed whenever user clicks on Save button in the Dialog window.
        /// </summary>
        /// <param name="sender"><c>Button</c>, that was clicked on.</param>
        /// <param name="e">Event arguments.</param>
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
