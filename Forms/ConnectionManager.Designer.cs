﻿namespace SSQLE
{
    partial class ConnectionManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cm_host_lbl = new System.Windows.Forms.Label();
            this.cm_host = new System.Windows.Forms.TextBox();
            this.cm_connect = new System.Windows.Forms.Button();
            this.cm_profiles = new System.Windows.Forms.ComboBox();
            this.cm_name = new System.Windows.Forms.TextBox();
            this.cm_name_lbl = new System.Windows.Forms.Label();
            this.cm_pass = new System.Windows.Forms.TextBox();
            this.cm_pass_lbl = new System.Windows.Forms.Label();
            this.cm_sid = new System.Windows.Forms.TextBox();
            this.cm_sid_lbl = new System.Windows.Forms.Label();
            this.cm_port = new System.Windows.Forms.TextBox();
            this.cm_port_lbl = new System.Windows.Forms.Label();
            this.cm_save = new System.Windows.Forms.Button();
            this.cm_delete = new System.Windows.Forms.Button();
            this.cm_user = new System.Windows.Forms.TextBox();
            this.cm_user_lbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cm_host_lbl
            // 
            this.cm_host_lbl.Location = new System.Drawing.Point(13, 67);
            this.cm_host_lbl.Name = "cm_host_lbl";
            this.cm_host_lbl.Size = new System.Drawing.Size(54, 13);
            this.cm_host_lbl.TabIndex = 0;
            this.cm_host_lbl.Text = "Host";
            this.cm_host_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cm_host
            // 
            this.cm_host.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cm_host.CausesValidation = false;
            this.cm_host.Location = new System.Drawing.Point(73, 65);
            this.cm_host.Name = "cm_host";
            this.cm_host.Size = new System.Drawing.Size(176, 20);
            this.cm_host.TabIndex = 3;
            // 
            // cm_connect
            // 
            this.cm_connect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cm_connect.Location = new System.Drawing.Point(13, 195);
            this.cm_connect.Name = "cm_connect";
            this.cm_connect.Size = new System.Drawing.Size(75, 23);
            this.cm_connect.TabIndex = 8;
            this.cm_connect.Text = "Connect";
            this.cm_connect.UseVisualStyleBackColor = true;
            this.cm_connect.Click += new System.EventHandler(this.cm_connect_Click);
            // 
            // cm_profiles
            // 
            this.cm_profiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cm_profiles.FormattingEnabled = true;
            this.cm_profiles.Location = new System.Drawing.Point(12, 12);
            this.cm_profiles.Name = "cm_profiles";
            this.cm_profiles.Size = new System.Drawing.Size(237, 21);
            this.cm_profiles.TabIndex = 1;
            this.cm_profiles.SelectedIndexChanged += new System.EventHandler(this.cm_profiles_SelectedIndexChanged);
            // 
            // cm_name
            // 
            this.cm_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cm_name.CausesValidation = false;
            this.cm_name.Location = new System.Drawing.Point(73, 39);
            this.cm_name.Name = "cm_name";
            this.cm_name.Size = new System.Drawing.Size(176, 20);
            this.cm_name.TabIndex = 2;
            // 
            // cm_name_lbl
            // 
            this.cm_name_lbl.Location = new System.Drawing.Point(12, 41);
            this.cm_name_lbl.Name = "cm_name_lbl";
            this.cm_name_lbl.Size = new System.Drawing.Size(55, 13);
            this.cm_name_lbl.TabIndex = 5;
            this.cm_name_lbl.Text = "Name";
            this.cm_name_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cm_pass
            // 
            this.cm_pass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cm_pass.CausesValidation = false;
            this.cm_pass.Location = new System.Drawing.Point(73, 117);
            this.cm_pass.Name = "cm_pass";
            this.cm_pass.PasswordChar = '*';
            this.cm_pass.Size = new System.Drawing.Size(176, 20);
            this.cm_pass.TabIndex = 5;
            // 
            // cm_pass_lbl
            // 
            this.cm_pass_lbl.Location = new System.Drawing.Point(13, 119);
            this.cm_pass_lbl.Name = "cm_pass_lbl";
            this.cm_pass_lbl.Size = new System.Drawing.Size(54, 13);
            this.cm_pass_lbl.TabIndex = 7;
            this.cm_pass_lbl.Text = "Password";
            this.cm_pass_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cm_sid
            // 
            this.cm_sid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cm_sid.CausesValidation = false;
            this.cm_sid.Location = new System.Drawing.Point(73, 143);
            this.cm_sid.Name = "cm_sid";
            this.cm_sid.Size = new System.Drawing.Size(176, 20);
            this.cm_sid.TabIndex = 6;
            // 
            // cm_sid_lbl
            // 
            this.cm_sid_lbl.Location = new System.Drawing.Point(13, 145);
            this.cm_sid_lbl.Name = "cm_sid_lbl";
            this.cm_sid_lbl.Size = new System.Drawing.Size(54, 13);
            this.cm_sid_lbl.TabIndex = 9;
            this.cm_sid_lbl.Text = "SID";
            this.cm_sid_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cm_port
            // 
            this.cm_port.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cm_port.CausesValidation = false;
            this.cm_port.Location = new System.Drawing.Point(73, 169);
            this.cm_port.Name = "cm_port";
            this.cm_port.Size = new System.Drawing.Size(176, 20);
            this.cm_port.TabIndex = 7;
            // 
            // cm_port_lbl
            // 
            this.cm_port_lbl.Location = new System.Drawing.Point(13, 171);
            this.cm_port_lbl.Name = "cm_port_lbl";
            this.cm_port_lbl.Size = new System.Drawing.Size(54, 13);
            this.cm_port_lbl.TabIndex = 11;
            this.cm_port_lbl.Text = "Port";
            this.cm_port_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cm_save
            // 
            this.cm_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cm_save.Location = new System.Drawing.Point(94, 195);
            this.cm_save.Name = "cm_save";
            this.cm_save.Size = new System.Drawing.Size(75, 23);
            this.cm_save.TabIndex = 9;
            this.cm_save.Text = "Save";
            this.cm_save.UseVisualStyleBackColor = true;
            this.cm_save.Click += new System.EventHandler(this.cm_save_Click);
            // 
            // cm_delete
            // 
            this.cm_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cm_delete.Location = new System.Drawing.Point(175, 195);
            this.cm_delete.Name = "cm_delete";
            this.cm_delete.Size = new System.Drawing.Size(75, 23);
            this.cm_delete.TabIndex = 10;
            this.cm_delete.Text = "Delete";
            this.cm_delete.UseVisualStyleBackColor = true;
            this.cm_delete.Click += new System.EventHandler(this.cm_delete_Click);
            // 
            // cm_user
            // 
            this.cm_user.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cm_user.CausesValidation = false;
            this.cm_user.Location = new System.Drawing.Point(73, 91);
            this.cm_user.Name = "cm_user";
            this.cm_user.Size = new System.Drawing.Size(176, 20);
            this.cm_user.TabIndex = 4;
            // 
            // cm_user_lbl
            // 
            this.cm_user_lbl.Location = new System.Drawing.Point(13, 93);
            this.cm_user_lbl.Name = "cm_user_lbl";
            this.cm_user_lbl.Size = new System.Drawing.Size(54, 13);
            this.cm_user_lbl.TabIndex = 12;
            this.cm_user_lbl.Text = "User";
            this.cm_user_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ConnectionManager
            // 
            this.AcceptButton = this.cm_connect;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 229);
            this.Controls.Add(this.cm_user);
            this.Controls.Add(this.cm_user_lbl);
            this.Controls.Add(this.cm_delete);
            this.Controls.Add(this.cm_save);
            this.Controls.Add(this.cm_port);
            this.Controls.Add(this.cm_port_lbl);
            this.Controls.Add(this.cm_sid);
            this.Controls.Add(this.cm_sid_lbl);
            this.Controls.Add(this.cm_pass);
            this.Controls.Add(this.cm_pass_lbl);
            this.Controls.Add(this.cm_name);
            this.Controls.Add(this.cm_name_lbl);
            this.Controls.Add(this.cm_profiles);
            this.Controls.Add(this.cm_connect);
            this.Controls.Add(this.cm_host);
            this.Controls.Add(this.cm_host_lbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConnectionManager";
            this.ShowIcon = false;
            this.Text = "Connections";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label cm_host_lbl;
        private System.Windows.Forms.TextBox cm_host;
        private System.Windows.Forms.Button cm_connect;
        private System.Windows.Forms.ComboBox cm_profiles;
        private System.Windows.Forms.TextBox cm_name;
        private System.Windows.Forms.Label cm_name_lbl;
        private System.Windows.Forms.TextBox cm_pass;
        private System.Windows.Forms.Label cm_pass_lbl;
        private System.Windows.Forms.TextBox cm_sid;
        private System.Windows.Forms.Label cm_sid_lbl;
        private System.Windows.Forms.TextBox cm_port;
        private System.Windows.Forms.Label cm_port_lbl;
        private System.Windows.Forms.Button cm_save;
        private System.Windows.Forms.Button cm_delete;
        private System.Windows.Forms.TextBox cm_user;
        private System.Windows.Forms.Label cm_user_lbl;
    }
}

