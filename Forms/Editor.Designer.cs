﻿namespace SSQLE
{
    partial class Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Editor));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.scintillaResults = new ScintillaNET.Scintilla();
            this.panelTop = new System.Windows.Forms.Panel();
            this.btn_clear = new System.Windows.Forms.Button();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asInsertStatementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.queryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scintillaInput = new ScintillaNET.Scintilla();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.btn_next_cmd = new System.Windows.Forms.Button();
            this.btn_previous_cmd = new System.Windows.Forms.Button();
            this.btn_execute = new System.Windows.Forms.Button();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusBarLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tooltip_execute = new System.Windows.Forms.ToolTip(this.components);
            this.btn_extractquery = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panelTop.SuspendLayout();
            this.menuBar.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.scintillaResults);
            this.splitContainer.Panel1.Controls.Add(this.panelTop);
            this.splitContainer.Panel1.Controls.Add(this.menuBar);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.scintillaInput);
            this.splitContainer.Panel2.Controls.Add(this.panelBottom);
            this.splitContainer.Size = new System.Drawing.Size(563, 506);
            this.splitContainer.SplitterDistance = 290;
            this.splitContainer.TabIndex = 0;
            this.splitContainer.TabStop = false;
            // 
            // scintillaResults
            // 
            this.scintillaResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scintillaResults.Location = new System.Drawing.Point(43, 24);
            this.scintillaResults.MouseSelectionRectangularSwitch = true;
            this.scintillaResults.Name = "scintillaResults";
            this.scintillaResults.ReadOnly = true;
            this.scintillaResults.Size = new System.Drawing.Size(520, 266);
            this.scintillaResults.TabIndex = 3;
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.btn_extractquery);
            this.panelTop.Controls.Add(this.btn_clear);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelTop.Location = new System.Drawing.Point(0, 24);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(43, 266);
            this.panelTop.TabIndex = 0;
            // 
            // btn_clear
            // 
            this.btn_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_clear.Location = new System.Drawing.Point(3, 226);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(37, 37);
            this.btn_clear.TabIndex = 3;
            this.btn_clear.Text = "Clear";
            this.tooltip_execute.SetToolTip(this.btn_clear, "Clears the output window.");
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // menuBar
            // 
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.loadQueryToolStripMenuItem});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(563, 24);
            this.menuBar.TabIndex = 2;
            this.menuBar.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resultToolStripMenuItem,
            this.queryToolStripMenuItem});
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // resultToolStripMenuItem
            // 
            this.resultToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asCSVToolStripMenuItem,
            this.asInsertStatementToolStripMenuItem});
            this.resultToolStripMenuItem.Name = "resultToolStripMenuItem";
            this.resultToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.resultToolStripMenuItem.Text = "Result";
            // 
            // asCSVToolStripMenuItem
            // 
            this.asCSVToolStripMenuItem.Name = "asCSVToolStripMenuItem";
            this.asCSVToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.asCSVToolStripMenuItem.Text = "As CSV";
            this.asCSVToolStripMenuItem.Click += new System.EventHandler(this.asCsvToolStripMenuItem_Click);
            // 
            // asInsertStatementToolStripMenuItem
            // 
            this.asInsertStatementToolStripMenuItem.Name = "asInsertStatementToolStripMenuItem";
            this.asInsertStatementToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.asInsertStatementToolStripMenuItem.Text = "As Insert Statement";
            this.asInsertStatementToolStripMenuItem.Click += new System.EventHandler(this.asInsertStatementToolStripMenuItem_Click);
            // 
            // queryToolStripMenuItem
            // 
            this.queryToolStripMenuItem.Name = "queryToolStripMenuItem";
            this.queryToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.queryToolStripMenuItem.Text = "Query";
            this.queryToolStripMenuItem.Click += new System.EventHandler(this.queryToolStripMenuItem_Click);
            // 
            // loadQueryToolStripMenuItem
            // 
            this.loadQueryToolStripMenuItem.Name = "loadQueryToolStripMenuItem";
            this.loadQueryToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.loadQueryToolStripMenuItem.Text = "Load Query";
            // 
            // scintillaInput
            // 
            this.scintillaInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scintillaInput.Location = new System.Drawing.Point(43, 0);
            this.scintillaInput.Name = "scintillaInput";
            this.scintillaInput.Size = new System.Drawing.Size(520, 212);
            this.scintillaInput.TabIndex = 1;
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.btn_next_cmd);
            this.panelBottom.Controls.Add(this.btn_previous_cmd);
            this.panelBottom.Controls.Add(this.btn_execute);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelBottom.Location = new System.Drawing.Point(0, 0);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(43, 212);
            this.panelBottom.TabIndex = 0;
            // 
            // btn_next_cmd
            // 
            this.btn_next_cmd.Enabled = false;
            this.btn_next_cmd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_next_cmd.Image = ((System.Drawing.Image)(resources.GetObject("btn_next_cmd.Image")));
            this.btn_next_cmd.Location = new System.Drawing.Point(3, 89);
            this.btn_next_cmd.Name = "btn_next_cmd";
            this.btn_next_cmd.Size = new System.Drawing.Size(37, 37);
            this.btn_next_cmd.TabIndex = 2;
            this.tooltip_execute.SetToolTip(this.btn_next_cmd, "Next command.");
            this.btn_next_cmd.UseVisualStyleBackColor = true;
            this.btn_next_cmd.Click += new System.EventHandler(this.btn_next_cmd_Click);
            // 
            // btn_previous_cmd
            // 
            this.btn_previous_cmd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_previous_cmd.Image = ((System.Drawing.Image)(resources.GetObject("btn_previous_cmd.Image")));
            this.btn_previous_cmd.Location = new System.Drawing.Point(3, 46);
            this.btn_previous_cmd.Name = "btn_previous_cmd";
            this.btn_previous_cmd.Size = new System.Drawing.Size(37, 37);
            this.btn_previous_cmd.TabIndex = 1;
            this.tooltip_execute.SetToolTip(this.btn_previous_cmd, "Preivous command.");
            this.btn_previous_cmd.UseVisualStyleBackColor = true;
            this.btn_previous_cmd.Click += new System.EventHandler(this.btn_previous_cmd_Click);
            // 
            // btn_execute
            // 
            this.btn_execute.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_execute.Image = ((System.Drawing.Image)(resources.GetObject("btn_execute.Image")));
            this.btn_execute.Location = new System.Drawing.Point(3, 3);
            this.btn_execute.Name = "btn_execute";
            this.btn_execute.Size = new System.Drawing.Size(37, 37);
            this.btn_execute.TabIndex = 0;
            this.tooltip_execute.SetToolTip(this.btn_execute, "Executes the query.");
            this.btn_execute.UseVisualStyleBackColor = true;
            this.btn_execute.Click += new System.EventHandler(this.btn_execute_Click);
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 484);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(563, 22);
            this.statusBar.SizingGrip = false;
            this.statusBar.TabIndex = 1;
            this.statusBar.Text = "statusStrip1";
            // 
            // statusBarLabel
            // 
            this.statusBarLabel.Name = "statusBarLabel";
            this.statusBarLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // btn_extractquery
            // 
            this.btn_extractquery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_extractquery.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_extractquery.Location = new System.Drawing.Point(3, 183);
            this.btn_extractquery.Name = "btn_extractquery";
            this.btn_extractquery.Size = new System.Drawing.Size(37, 37);
            this.btn_extractquery.TabIndex = 4;
            this.btn_extractquery.Text = "Ext";
            this.tooltip_execute.SetToolTip(this.btn_extractquery, "Extracts result of last query to separate window.");
            this.btn_extractquery.UseVisualStyleBackColor = true;
            this.btn_extractquery.Click += new System.EventHandler(this.btn_extractquery_Click);
            // 
            // Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 506);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.splitContainer);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(400, 250);
            this.Name = "Editor";
            this.Text = "SSQLE";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Editor_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Editor_KeyDown);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.panelBottom.ResumeLayout(false);
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.ToolStripStatusLabel statusBarLabel;
        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private ScintillaNET.Scintilla scintillaInput;
        private System.Windows.Forms.Button btn_execute;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asCSVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asInsertStatementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem queryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadQueryToolStripMenuItem;
        private System.Windows.Forms.ToolTip tooltip_execute;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button btn_next_cmd;
        private System.Windows.Forms.Button btn_previous_cmd;
        private ScintillaNET.Scintilla scintillaResults;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_extractquery;
    }
}