﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScintillaNET;

namespace SSQLE.Forms
{
    public partial class QueryResultWindow : Form
    {
        public QueryResultWindow()
        {
            InitializeComponent();
            scintilla_queryresult.Margins[1].Width = 0;
            scintilla_queryresult.Styles[Style.Default].Font = "Courier New";
            scintilla_queryresult.Styles[Style.Default].Size = 10;
        }

        public QueryResultWindow(QueryResult queryResult) : this()
        {
            scintilla_queryresult.Text = queryResult.ToString();
        }
    }
}
