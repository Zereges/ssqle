SSQLE stands for Simple SQL Editor, that is a GUI application providing facilities for executing queries into SQL based database.

Even though there are definitely many IDEs for working in SQL based environment, there is still a feature, that no IDE can provide. That is simplicity. Every IDE has lot of features, but sometimes, you do not need them. You just want to start the editor, connect to a database, execute few queries and that's it. No need for autocompletion, data visualisation, object browser, unit testing or any others, altough great, but not needed features.

### Features
- Estabilishing a connection to Oracle based database and eventually save it for later use.
- Execute a SQL query and show the result in plain text.
- Export the result of the query to CSV or as SQL `INSERT` statement.
- Lexical highlighting in query editor (keywords, functions, literals)
- Commands history and option to re-run previous commands.
- Saving favourite queries as snippets.
- Controllable using keyboard shortcuts.
- Ability to change options using XML file with user profile.

With little bit of programming, one can extend the functionality to provide connection to any other SQL based databse. See [IConnection](http://www.zereges.cz/SSQLE/doc/interface_s_s_q_l_e_1_1_i_connection.html) for information how to extend the functionality.

This application can be run under Mono, but [Scintilla.NET](https://github.com/jacobslusser/ScintillaNET) is not (yet?) ported for Mono on Linux. One can change Scintilla components to basic TextBox-es to preserve functionality of connecting to database, with sacrifice of features of Scintilla (mainly used syntax highlighting and block selection).

### Download
##### Release
[SSQLE 1.2](http://www.zereges.cz/SSQLE/SSQLE-1.2-bin.zip) with all libraries required. Run and go, if you have **.NET4.5**

##### Source
[SSQLE 1.2](http://www.zereges.cz/SSQLE/SSQLE-1.2-src.zip) contains source code used to build release 1.2.

You can however clone the repository yourself. See Compilation section of this manual. Beware, that source code in the repository might be in development, thus unstable, crashing or whatever else.

### Compilation
SSQLE was developed using Visual Studio 2015. It uses following libraries

- ODP.NET (Oracle Data Provider for .NET) from [NuGet](https://www.nuget.org/packages/Oracle.ManagedDataAccess.EntityFramework/)
- Scintilla.NET from [GitHub](https://github.com/jacobslusser/ScintillaNET).

ODP.NET will be automaticaly downloaded by Visual Studio, but you have to download Scintilla.NET by yourself. Either try release or compile your own. Place it to `packages/Scintilla.NET` folder.

Compilation is done using Visual Stuio MSBuild automaticaly by pressing *Build Solution*. Resulting file (with all DLLs copied) will be in `bin/Release` folder (Or depending on project configuration).

Programmers doxygen generated documentation can be found on [this](http://www.zereges.cz/SSQLE/doc/) link.

### SSQLE
First displayed form is Connection manager. It allows you to create, use, modify, save or delete configuration profiles. If there is at least one custom profile, SSQLE will sort them by time of last usage in descending order (thus you will have latest used selected on the top). The controls are intuitive.

When you connect, the editor will establish a connection to a database and brin up second form -- Editor.  
In bottom box, you write queries and their results are shown in top box.

In *File* menu, there is just *Close* button.  
In *Save* menu, you can choose to save result of the query to either *CVS* or as *SQL `INSERT` statement*. Or you can save a query as a snippet and call it back lagter using *Load* menu.  
Topmost button next to query editor is *Execute* which will execute currently written query (shortcut **CTRL-Enter**)  
Under it, there is *Previous command*  (shortcut **Ctrl-UP**, equivalent to bash Up arrow for history traversal).  
Last button is *Next command*  (shortcut **Ctrl-Down**, equivalent to bash Down arrow for history traversal).  

### Settings
SSQLE is a bit customizable, you can change settings by editing `settings/name.xml` file in application root directory, representing your previously saved profile. Contents of the file are parsed and validated using `System.XML` .NET. It is validated against `settings.xsd` schema located in root directory of the application. If validation fails for any reason, the settings won't be loaded, so be cautious when editing a file.

One can look at the validation requirements into XML Schema file, but here is quick reference

- `settings/name` is name of the connection, it is wise to leave it as is (value should be same as name of the file + XML extension).
 - `settings/host` is address of the server
 - `settings/user` is username to be used as login
 - `settings/pass` is encrypted (defualt by trivial encryption, see [class in documentation](http://www.zereges.cz/SSQLE/doc/class_s_s_q_l_e_1_1_trivial_crypto.html) password.
 - `settings/sid` is Oracle SID (database identifier)
 - `settings/port` is port to connect to.
 - `settings/highlighter` contains settings for the highlighter. One can change settings how *keyword*, *predefined function*, *comment*, *string literal* and *number literal* are rendered. *bold* and *italic* attributes are self-explainatory and value of the XML node is color, see [documentation](http://www.zereges.cz/SSQLE/doc/class_s_s_q_l_e_1_1_color_extensions.html).
 - `settings/history` contains previously executed queries
 - `settings/snippets` contains saved queries.

### License
The MIT License (MIT)

Copyright (c) 2016 Zereges

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.